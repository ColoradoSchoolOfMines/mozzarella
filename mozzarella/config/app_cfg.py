"""
File for configuration **defaults** in Mozzarella.

Also specifies authentication methods.

The defaults should be overridden by the paste config
(``development.ini`` or ``production.ini``).

From the TG docs:

.. tip::

    A good indicator of whether an option should be set in the config
    directory code vs. the configuration file is whether or not the
    option is necessary for the functioning of the application. If the
    application won't function without the setting, it belongs in the
    appropriate ``config`` directory file. If the option should be
    changed depending on deployment, it belongs in the ``ini`` files.

"""
import sys

import tg.predicates
from depot.manager import DepotManager
from tg import config, request
from tg.configuration import AppConfig
from tgext.admin.tgadminconfig import BootstrapTGAdminConfig as TGAdminConfig

import mozzarella
from mozzarella.config.auth import (APITokenAuthenticator, AuthMetadata,
                                    MPAPIAuthenticator)

base_config = AppConfig()
base_config.renderers = []

# True to prevent dispatcher from striping extensions
# For example /socket.io would be served by "socket_io"
# method instead of "socket".
base_config.disable_request_extensions = False

# Set None to disable escaping punctuation characters to "_"
# when dispatching methods.
# Set to a function to provide custom escaping.
base_config.dispatch_path_translator = True

base_config.prefer_toscawidgets2 = True

base_config.package = mozzarella

# Enable json in expose
base_config.renderers.append('json')

# Set the default renderer
base_config.renderers.append('kajiki')
# Change this in setup.py too for i18n to work.
base_config['templating.kajiki.strip_text'] = False

base_config.default_renderer = 'kajiki'


# Configure Sessions, store data as JSON to avoid pickle security issues
base_config['session.enabled'] = True
base_config['session.data_serializer'] = 'json'

# Configure the base SQLALchemy Setup
base_config.use_sqlalchemy = True
base_config.model = mozzarella.model
base_config.DBSession = mozzarella.model.DBSession

# Configure the authentication backend
base_config.auth_backend = 'sqlalchemy'
# YOU MUST CHANGE THIS VALUE IN PRODUCTION TO SECURE YOUR APP
base_config.sa_auth.cookie_secret = "2a654f87-1cb4-43fb-834b-413c449a71a8"
base_config.sa_auth.authmetadata = AuthMetadata(base_config.sa_auth)
# In case AuthMetadata didn't find the user discard the whole identity.  This
# might happen if logged-in users are deleted.
base_config['identity.allow_missing_user'] = False

mpapi_authenticator = MPAPIAuthenticator()

# You can use a different repoze.who Authenticator if you want to
# change the way users can login
base_config.sa_auth.authenticators = [
    ('token', APITokenAuthenticator()),
    ('mpapi', mpapi_authenticator),
]

# You can add more repoze.who metadata providers to fetch
# user metadata.
# Remember to set base_config.sa_auth.authmetadata to None
# to disable authmetadata and use only your own metadata providers
# base_config.sa_auth.mdproviders = [('myprovider', SomeMDProvider()]

base_config['auth.mpapi.url'] = 'https://mastergo.mines.edu/mpapi'

# override this if you would like to provide a different who plugin for
# managing login and logout of your application
base_config.sa_auth.form_plugin = mpapi_authenticator

# You may optionally define a page where you want users to be redirected to
# on login:
base_config.sa_auth.post_login_url = '/post_login'


# Admin configuration
class AdminConfig(TGAdminConfig):
    allow_only = tg.predicates.has_permission('admin')


# Variable provider: this provides a default set of variables to the templating
# engine
def variable_provider():
    d = {}
    if request.identity:
        d['luser'] = request.identity['user']
    else:
        d['luser'] = None
    return d


base_config.variable_provider = variable_provider


def config_ready():
    """ Executed once the configuration is ready. """
    # don't run when setting up the database
    if 'setup-app' in sys.argv:
        return

    import tgscheduler
    tgscheduler.start_scheduler()

    # Configure default depot
    DepotManager.configure('default', tg.config)

    # Schedule up some syncing with pipermail
    if config.get('mailman.pipermail.enable') == 'True':
        from mozzarella.lib.pipermailsync import pmsync
        tgscheduler.scheduler.add_interval_task(action=pmsync, initialdelay=0, interval=5*60)

    # Use CSS/JS/images from site
    config.paths['static_files'] = 'mozzarella/sites/' + config['site'] + '/public'


from tg.configuration import milestones

milestones.config_ready.register(config_ready)

try:
    # Enable DebugBar if available, install tgext.debugbar to turn it on
    from tgext.debugbar import enable_debugbar
    enable_debugbar(base_config)
except ImportError:
    pass
