"""Defines the Meeting model.

Meetings are points in time when the club is meeting.
"""

import datetime

from tg import config

from sqlalchemy import Column, ForeignKey
from sqlalchemy.orm import relation
from sqlalchemy.types import DateTime, Integer, Text, Unicode

from mozzarella.model import DeclarativeBase


class Meeting(DeclarativeBase):
    """A model that defines a club meeting.

    Meetings are displayed on the schedule/calendar page"""
    __tablename__ = 'meeting'

    id = Column(Integer, primary_key=True)
    date = Column(DateTime, nullable=False)
    _duration = Column(Integer, nullable=True)
    location = Column(Text)
    title = Column(Unicode, nullable=False)
    description = Column(Unicode)
    survey_id = Column(Integer, ForeignKey('survey.id'),
                       nullable=True, unique=True)
    survey = relation("Survey", back_populates="meeting")
    more_info = Column(Unicode)  # appears only on the detail page

    def __str__(self):
        return '{} ({})'.format(self.title, self.date)

    def __repr__(self):
        return "<Meeting title='{}' date='{}'>".format(self.title, self.date)

    @property
    def duration(self):
        """Return the meetings duration as a ``datetime.timedelta``."""
        return datetime.timedelta(seconds=self._duration or
                                  int(config.get('meetings.default_duration')))

    @property
    def has_specific_duration(self):
        """
        Returns ``True`` if the meeting has a specified duration,
        ``False`` otherwise.
        """
        return bool(self._duration)
