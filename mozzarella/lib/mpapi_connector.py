from urllib.parse import quote

import requests

mpapi_base = 'https://mastergo.mines.edu/mpapi/'


class InvalidUsername(Exception):
    pass


def uidinfo(username):
    r = requests.get(mpapi_base + 'uid/' + quote(username, safe=''))
    r.raise_for_status()
    data = r.json()
    if data["result"] != "success":
        raise InvalidUsername("Bad Mines Username")
    return data["attributes"]
