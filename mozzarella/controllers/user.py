"""Profile controller module"""
from tg import expose, redirect, abort, url, require, request
from tg.predicates import Predicate, not_anonymous
from depot.manager import DepotManager

from mozzarella.lib.base import BaseController
from mozzarella.model import DBSession, User
from mozzarella.model.auth import ProfileVisibilityOptions
from mozzarella.lib.card import Card, CardTypes

__all__ = ['UsersController']


def card_about_me(user):
    if user.bio:
        yield Card("About Me", dict(text=user.bio))


cards = CardTypes()
cards.register(card_about_me, body_template="cards/bio_body.xhtml")


def user_error_404():
    abort(404, "User not found")


def check_permissions(self):
    requesting_user = request.identity and request.identity.get('user')
    if self.user.profile_visibility == ProfileVisibilityOptions.everyone:
        return True
    if self.user.profile_visibility == ProfileVisibilityOptions.authenticated_users:
        return requesting_user is not None
    if self.user.profile_visibility == ProfileVisibilityOptions.only_me:
        return requesting_user is self.user
    return False


def is_user_allowed(f):
    def magic(self):
        if check_permissions(self):
            return f(self)
        else:
            user_error_404()
    return magic


class UserController(BaseController):
    def __init__(self, user):
        self.user = user

    @expose('mozzarella.templates.profile')
    @is_user_allowed
    def _default(self):
        """Handle the user's profile page."""
        return dict(page='profile', u=self.user,
                    cardlist=cards.generate_cards(self.user))

    @expose()
    def picture(self):
        if check_permissions(self) and self.user.profile_pic:
            redirect(DepotManager.url_for(self.user.profile_pic.path))
        else:
            redirect(url('/img/default_user.png'))

    @expose('mozzarella.templates.profile_edit')
    @require(not_anonymous(msg='Only logged in users can edit profiles'))
    def edit(self):
        requesting_user = request.identity and request.identity.get('user')
        if requesting_user is self.user:
            return dict(page='profile_edit', u=self.user)
        user_error_404()


class NonExistentUserController(BaseController):
    @expose()
    def _default(self):
        user_error_404()

    @expose()
    def picture(self):
        redirect(url('/img/default_user.png'))

    @expose()
    def edit(self):
        user_error_404()


class UsersController(BaseController):
    @expose()
    def _lookup(self, uname=None, *args):
        user = None
        if uname:
            user = DBSession.query(User) \
                            .filter(User.user_name == uname) \
                            .one_or_none()
        if not user:
            return NonExistentUserController(), args

        return UserController(user), args
