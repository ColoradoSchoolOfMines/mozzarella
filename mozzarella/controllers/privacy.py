"""
Privacy settings UI
"""
from mozzarella.lib.base import BaseController
from mozzarella.model.auth import ProfileVisibilityOptions

from tg import abort, expose, request, require, flash, redirect
from tg.predicates import not_anonymous


class PrivacyController(BaseController):
    @require(not_anonymous(msg='You must be logged in to edit your privacy settings'))
    @expose('mozzarella.templates.privacy')
    def index(self):
        form = request.POST
        user = request.identity and request.identity.get('user')
        if form:
            new_visibility = form.get('profile_visibility')
            if new_visibility is None:
                abort(400, 'Invalid profile_visibility')
            user.profile_visibility = ProfileVisibilityOptions[new_visibility]
            flash("Privacy preferences successfully updated.")
            redirect('/privacy')
        else:
            return dict(profile_visibility=user.profile_visibility,
                        ProfileVisibilityOptions=ProfileVisibilityOptions)
