"""Discord controller module"""

from tg import expose

from mozzarella.lib.base import BaseController


class DiscordController(BaseController):
    """Controller for Discord page"""

    @expose('mozzarella.templates.discord')
    def index(self):
        return dict(page='discord')
