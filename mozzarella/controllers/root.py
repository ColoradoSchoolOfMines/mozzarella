"""Main Controller"""
import datetime
from importlib import import_module

from repoze.who.api import get_api
from sqlalchemy.sql import functions
from tg import (abort, config, expose, flash, lurl, redirect, request, require,
                session, tmpl_context, url)
from tg.exceptions import HTTPFound
from tg.i18n import ugettext as _
from tg.predicates import not_anonymous
from tgext.admin.controller import AdminController

from mozzarella import model
from mozzarella.config.app_cfg import AdminConfig
from mozzarella.controllers.contact import ContactController
from mozzarella.controllers.discord import DiscordController
from mozzarella.controllers.error import ErrorController
from mozzarella.controllers.mailinglist import MailingListController
from mozzarella.controllers.matrix import MatrixController
from mozzarella.controllers.meeting import MeetingsController
from mozzarella.controllers.presentations import PresentationsController
from mozzarella.controllers.privacy import PrivacyController
from mozzarella.controllers.project import ProjectsController
from mozzarella.controllers.schedule import ScheduleController
from mozzarella.controllers.survey import SurveysController
from mozzarella.controllers.user import UsersController
from mozzarella.lib.base import BaseController
from mozzarella.model import Banner, DBSession, Meeting, Survey

__all__ = ['RootController']


class RootController(BaseController):
    """
    The root controller for Mozzarella.

    All the other controllers and WSGI applications should be mounted on this
    controller. For example::

        panel = ControlPanelController()
        another_app = AnotherWSGIApplication()

    Keep in mind that WSGI applications shouldn't be mounted directly: They
    must be wrapped around with :class:`tg.controllers.WSGIAppController`.

    """
    admin = AdminController(model, DBSession, config_type=AdminConfig)
    mailinglist = MailingListController()
    u = UsersController()
    m = MeetingsController()
    s = SurveysController()
    schedule = ScheduleController()
    discord = DiscordController()
    error = ErrorController()
    contact = ContactController()
    projects = ProjectsController()
    presentations = PresentationsController()
    matrix = MatrixController()
    privacy = PrivacyController()

    def __init__(self):
        self.site = import_module('mozzarella.sites.' + config['site'])
        if hasattr(self.site, 'controllers'):
            for route, con in self.site.controllers.items():
                setattr(self, route, con)
        super(RootController, self).__init__()

    def _before(self, *args, **kw):
        tmpl_context.project_name = "Mozzarella"
        if hasattr(self.site, 'default_theme') and session.get('theme') is None:
            session['theme'] = self.site.default_theme
            session.save()

    @expose('mozzarella.sites.' + config['site'] + '.templates.index')
    def index(self):
        """Handle the front-page."""
        meetings = DBSession.query(Meeting).filter(
            Meeting.date > datetime.datetime.now() - datetime.timedelta(hours=3)
        ).order_by(Meeting.date).limit(2)
        banner = DBSession.query(Banner).order_by(functions.random()).first()
        return dict(page='index', meetings=meetings, banner=banner)

    @expose()
    def login(self):
        if not request.identity:
            who_api = get_api(request.environ)
            return who_api.challenge()
        redirect(url('/'))

    @expose()
    @require(not_anonymous())
    def logout(self):
        who_api = get_api(request.environ)
        headers = who_api.logout()
        return HTTPFound(headers=headers)

    @expose()
    def post_login(self, came_from=lurl('/')):
        """post_login(came_from=tg.lurl('/'))

        Redirect the user to the initially requested page on successful
        authentication or redirect her back to the login page if login failed.
        """
        if not request.identity:
            flash('Login failure')
            redirect('/')

        user = request.identity['user']
        flash(_('Welcome back, %s!') % user.display_name, 'success')
        u = request.relative_url(str(came_from),
                                 to_application=True)
        if not u.startswith(request.application_url):
            flash("Dangerous redirect prevented", "warning")
            redirect('/')
        redirect(u)

    @expose()
    def toggle_theme(self):
        if session.get('theme', None) == 'dark':
            session['theme'] = 'light'
        else:
            session['theme'] = 'dark'
        session.save()
        return session.get('theme', None)

    @expose()
    def attend(self):
        curr_time = datetime.datetime.now()
        meeting = DBSession.query(Meeting)\
                           .join(Meeting.survey)\
                           .filter(
                               Survey.opens < curr_time,
                               Survey.closes > curr_time
                           ).order_by(Meeting.date.desc()).first()

        if meeting and meeting.survey.active:
            redirect('s/{}/respond'.format(meeting.survey.id))
        else:
            abort(404, 'No active meeting')
