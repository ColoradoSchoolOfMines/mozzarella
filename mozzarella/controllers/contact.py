"""Contact controller module"""

from tg import expose

from mozzarella.model import DBSession
from mozzarella.model.auth import User, OfficerTitle

from mozzarella.lib.base import BaseController


class ContactController(BaseController):
    @expose('mozzarella.templates.contact')
    def index(self):
        """Handle the 'contact' page."""
        officers = (DBSession.query(User).join(OfficerTitle)
                    .order_by(OfficerTitle.rank).all())
        return dict(page='contact', officers=officers)
