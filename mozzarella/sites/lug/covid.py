"""HelpSessions controller module"""

from tg import expose

from mozzarella.lib.base import BaseController


class COVIDController(BaseController):
    """Controller for help sessions page"""

    @expose('mozzarella.sites.lug.templates.covid')
    def index(self):
        return dict(page="covid")
