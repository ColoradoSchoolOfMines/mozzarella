(function($){

    function display_valid(ok, item) {
        var p = $(item).closest('.form-group');
        var i = $(item).siblings('span.glyphicon');
        if (ok) {
            $(p).removeClass('has-error');
            $(p).addClass('has-success');
            $(i).removeClass('glyphicon-remove');
            $(i).addClass('glyphicon-ok');
        }
        else {
            $(p).removeClass('has-success');
            $(p).addClass('has-error');
            $(i).removeClass('glyphicon-ok');
            $(i).addClass('glyphicon-remove');
        }
        refresh_button();
    }

    function reset_valid() {
        var p = $(this).closest('.form-group');
        var i = $(this).siblings('span.glyphicon');
        $(p).removeClass('has-error');
        $(p).removeClass('has-success');
        $(i).removeClass('glyphicon-remove');
        $(i).removeClass('glyphicon-ok');
        refresh_button();
    }

    function refresh_button () {
        var ok = true;
        ['email_addr', 'display_name', 'username', 'password', 'confirm_password']
            .forEach(function (item) {
                if (!$("#" + item).siblings('span.glyphicon').hasClass('glyphicon-ok'))
                    ok = false;
            });
        $("#continue-btn").prop("disabled", !ok);
    }

    $("#email_addr")
        .blur(function () {
            var ok = /^[A-Za-z0-9\.\+]+@(mymail\.)?mines\.edu$/.test($(this).val());
            var field = this;
            if (ok) {
                $.getJSON('/register/in_use/email_addr?value=' + $(this).val())
                    .done(function (json) {
                        display_valid(!json.result, $(field));
                        if (json.result)
                            $(field).siblings('p.error-msg').show();
                        else
                            $(field).siblings('p.error-msg').hide();
                    });
            }
            else {
                display_valid(false, $(this));
            }
        })
    .focus(reset_valid);

    $("#display_name")
        .blur(function () {
            var ok = /^.{1,255}$/.test($(this).val());
            display_valid(ok, $(this));
        })
    .focus(reset_valid);

    $("#username")
        .blur(function () {
            var ok = /^[A-Za-z0-9_]{1,16}$/.test($(this).val());
            var field = this;
            if (ok) {
                $.getJSON('/register/in_use/username?value=' + $(this).val())
                    .done(function (json) {
                        display_valid(!json.result, $(field));
                        if (json.result)
                            $(field).siblings('p.error-msg').show();
                        else
                            $(field).siblings('p.error-msg').hide();
                    });
            }
            else {
                display_valid(false, $(this));
            }
        })
    .focus(reset_valid);

    $("#password")
        .blur(function () {
            var ok = /^.{8,255}$/.test($(this).val());
            display_valid(ok, $(this));
        })
    .focus(reset_valid);

    $("#confirm_password")
    .blur(function () {
        var ok = $(this).val() == $("#password").val();
        $(this).siblings('p.error-msg').css("display", ok?"none":"unset");
        display_valid(ok, $(this));
    })
    .focus(reset_valid)
    .on("change keyup paste", function() {
        var ok = $(this).val() == $("#password").val();
        if (ok) display_valid(1, $(this));
    });

    $("#continue-btn").click(function(event) {
        event.preventDefault();
        $('#validation-modal').modal('show');
        $.post('/register/send_validation_email', {email_addr: $('#email_addr').val(), display_name: $('#display_name').val()});
    });

})(jQuery);
