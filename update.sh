#!/bin/sh
# this script is used by the sysadmin deploying mozzarella to update
# dependencies in the venv
PIPENV_VENV_IN_PROJECT=1 pipenv sync --dev
