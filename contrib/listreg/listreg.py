import requests
import sys
from bs4 import BeautifulSoup
import time

url = sys.argv[1]
banner = ''

if len(sys.argv) > 2:
    with open(sys.argv[2]) as f:
        banner = f.read()

while True:
    print(banner)
    uname = input("What is your Mines user name? ")
    if '@' in uname:
        print('Only a username (not email address) is required.')
    else:
        print('Subscribing...')
        r = requests.post(url, data={'ml_username': uname, 'ml_fullname': ''})
        if r.ok:
            soup = BeautifulSoup(r.text, 'html.parser')
            print(soup.find(id='flash').get_text())
        else:
            print('ERROR: Network error')
    time.sleep(2.5)
    print('\x1b[2J')
