"""
Dead projects

Revision ID: 6361663a5f78
Revises: b14dffc4351e
Create Date: 2019-11-06 12:47:25.577838

"""

from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = '6361663a5f78'
down_revision = 'b14dffc4351e'


def upgrade():
    op.add_column('projects', sa.Column('dead', sa.Boolean(), default=False,
                                        server_default='FALSE', nullable=False))


def downgrade():
    op.drop_column('projects', 'dead')
