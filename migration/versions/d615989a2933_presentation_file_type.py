"""
Rename presentation file description column to type

Revision ID: d615989a2933
Revises: 6361663a5f78
Create Date: 2020-01-16 10:05:44.816335

"""

from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = 'd615989a2933'
down_revision = '6361663a5f78'


def upgrade():
    op.alter_column('presentation_file', 'description', new_column_name='type')


def downgrade():
    op.alter_column('presentation_file', 'type', new_column_name='description')
