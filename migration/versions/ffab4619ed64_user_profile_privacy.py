"""
User profile privacy

Revision ID: ffab4619ed64
Revises: e0f30c4c92e9
Create Date: 2020-09-26 12:34:00.461109

"""

from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = 'ffab4619ed64'
down_revision = 'e0f30c4c92e9'

profile_visibility_enum = sa.Enum('only_me', 'authenticated_users', 'everyone',
                                  name='ProfileVisibilityOptions')


def upgrade():
    profile_visibility_enum.create(op.get_bind())
    op.add_column('tg_user', sa.Column('profile_visibility',
                                       profile_visibility_enum,
                                       server_default='only_me',
                                       nullable=False))


def downgrade():
    op.drop_column('tg_user', 'profile_visibility')
